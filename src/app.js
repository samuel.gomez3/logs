const AWS = require('aws-sdk');


AWS.config.update({
    accessKeyId: process.env.AWS_ACCESS_KEY_ID,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
    region: 'us-west-1'
});

const sqs = new AWS.SQS();
const QUEUE_URL = "https://sqs.us-east-1.amazonaws.com/906647043531/logs";

function processMessage(message) {
    console.log( message.Body);


    const deleteParams = {
        QueueUrl: QUEUE_URL,
        ReceiptHandle: message.ReceiptHandle
    };
    sqs.deleteMessage(deleteParams, (deleteErr, deleteData) => {
        if (deleteErr) {
            console.error("Error al eliminar el mensaje", deleteErr);
        } else {
            console.log("Mensaje eliminado:", message.MessageId);
        }
    });
}

function pollQueue() {
    const params = {
        QueueUrl: QUEUE_URL,
        MaxNumberOfMessages: 1,
        VisibilityTimeout: 60,
        WaitTimeSeconds: 20
    };

    sqs.receiveMessage(params, (err, data) => {
        if (err) {
            console.error("Error al recibir el mensaje", err);
        } else {
            if (data.Messages && data.Messages.length > 0) {
                const message = data.Messages[0];
                processMessage(message);
            }

            pollQueue();
        }
    });
}

pollQueue();
